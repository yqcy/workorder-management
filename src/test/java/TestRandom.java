import org.apache.poi.ss.formula.functions.T;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;

public class TestRandom {
    @Test
    public void testRandom() {
        Random random = new Random();
    }

    static class Parent{
        static{
            System.out.println("parent static");
        }

        public Parent() {
            System.out.println("parent");
        }
    }

    static class Child extends Parent{
        public int i;

        static{
            System.out.println("child static");
        }

        public Child() {
            i = 11;
            System.out.println("child");
        }
        {
            i = 10;
        }
    }

    @Test
    public void testExtends() {
        Child child = new Child();
        Child child1 = new Child();
        System.out.println(child.i);
    }

    @Test
    public void testOptional() {
        String str = null;
        // Optional.ofNullable(str).map();
    }

    class Fruit{
        @Override
        public String toString() {
            return "fruit";
        }
    }

    class Apple extends Fruit{
        @Override
        public String toString() {
            return "apple";
        }
    }

    class Plate<T extends Fruit>{
        private T value;

        public void set(T t) {
            this.value = t;
        }

        public T get() {
            return this.value;
        }

        public Plate(){

        }

        public Plate(T t) {
            this.value = t;
        }
    }

    @Test
    public void testExtendsSuper() {
        Plate<? extends Fruit> plate = new Plate(new Fruit());
        Plate<? extends Fruit> plate2 = new Plate(new Apple());
        Plate<? super Fruit> plate3 = new Plate();
        System.out.println(plate.get());
        System.out.println(plate2.get());
        plate3.set(new Fruit());
        System.out.println(plate3.get());
    }

    @Test
    public void testMath() {
        System.out.println("floor");
        System.out.println(Math.floor(1.4));
        System.out.println(Math.floor(1.5));
        System.out.println(Math.floor(-1.4));
        System.out.println(Math.floor(-1.5));
        System.out.println(Math.floor(-1.6));
        System.out.println(Math.floor(1.0));
        System.out.println(Math.floor(-1.0));

        System.out.println("round");
        System.out.println(Math.round(1.4));
        System.out.println(Math.round(1.5));
        System.out.println(Math.round(-1.4));
        System.out.println(Math.round(-1.5));
        System.out.println(Math.round(-1.6));

        System.out.println("ceil");
        System.out.println(Math.ceil(1.4));
        System.out.println(Math.ceil(1.5));
        System.out.println(Math.ceil(-1.4));
        System.out.println(Math.ceil(-1.5));
        System.out.println(Math.ceil(-1.6));
        System.out.println(Math.ceil(1.1));
    }


}
