import javafx.beans.binding.When;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * created by wb-yq264139 on 2017/11/20
 */
public class TestString {

    @Test
    public void testTrim() {
        String s = " hehe";

        String s1 = "haha ";

        String s2 = " wawa     ";

        System.out.println(s.trim() + "\n" + s1.trim() + "\n" + s2.trim());
    }

    @Test
    public void testString() {
        // 此时str1在堆中，常量池中还没有abc123这个字符串
        String str1 = new StringBuilder("abc").append("123").toString();
        // 调用intern，该方法会从常量池中使用equals判断，true直接返回，false先创建再返回
        // 可以看官方的介绍：
        // When the intern method is invoked, if the pool already contains a string equal to this String object as determined by the equals(Object) method, then the string from the pool is returned. Otherwise, this String object is added to the pool and a reference to this String object is returned.
        // 此时常量池中已经包含abc123
        str1.intern();
        // 重新创建一个变量str2并赋值，此时常量池中已经存在abc123，该str2是常量池的引用
        // 如果：
        // 常量池中存的是对象，那么str2的abc123是常量池的对象地址，str1的abc123是堆中的地址
        // str1 == str2理想的结果应该是false
        String str2 = "abc123";
        // 实际返回true
        // str1和str2在内存中同一地址，说明常量池中存的是对象的引用，对象实际还是在堆中管理
        System.out.println(str1 == str2);
    }

    @Test
    public void test1() {
        String s = "a";
        String s1 = s + "b";
        // s1.intern();
        System.out.println("ab" == s1);
    }

    @Test
    public void testInt() {
        Integer a = -128;
        Integer b = -128;
        Integer c = -129;
        Integer d = -129;
        System.out.println(a == b);
        System.out.println(c == d);
    }

    @Test
    public void testRef() {
        StringBuilder sb = new StringBuilder("init");
        add(sb);
        System.out.println(sb.toString());
        modifyRef(sb);
        System.out.println(sb.toString());
    }

    void add(StringBuilder sb) {
        sb.append("_add");
    }

    void modifyRef(StringBuilder sb) {
        sb = new StringBuilder("init2");
    }

    @Test
    public void testStringCreation() {
        String s1 = new String("abc");
        String s4 = s1.intern();
        String s2 = "abc";
        String s3 = new String("abc");
        System.out.println(s4 == s2);
        ExecutorService pool = Executors.newFixedThreadPool(10);
    }


}
