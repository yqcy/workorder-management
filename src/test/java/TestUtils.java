import com.alibaba.fastjson.JSON;
import com.yq.entity.User;
import com.yq.entity.dto.Peo;
import com.yq.util.DateUtils;
import org.junit.Test;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Math.random;

/**
 * created by wb-yq264139 on 2017/11/13
 */
public class TestUtils {

    @Test
    public void testCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.YEAR, -10);
        Date time = calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf.format(time);
        System.out.println(format);
    }

    @Test
    public void testDateUtils() {
        DateUtils dateUtils = new DateUtils();
        dateUtils.init();
        Date parse = dateUtils.parse("1999-10-10", "yyyy-MM-dd");
        System.out.println(parse);
    }


    @Test
    public void testJSON() {
//        ArrayList<Object> objects = new ArrayList<>();
//        String body = JSON.toJSONString(objects, SerializerFeature.PrettyFormat);
//        System.out.println(body);

        Peo peo = JSON.parseObject("{\"name\":\"yq\"}", Peo.class);
        System.out.println(peo.name);
    }

    @Test
    public void testUser() {
        System.out.println(1 >> 1);
    }
    // 第一题
    @Test
    public void testIP2Long() {
        String ips = "192.68.0.1 , 192.189.2.1, 10.189.2.1 , 10.10.2.1";
        String[] ipArray = ips.split(",");
        List<Long> list = new ArrayList<>();
        String pattern = "(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)";
        Pattern r = Pattern.compile(pattern);
        for (String s : ipArray) {
            Matcher m = r.matcher(s.trim());
            if (m.find()) {
                list.add((Long.parseLong(m.group(1)) << 24) + (Long.parseLong(m.group(2)) << 16) + (Long.parseLong(m.group(3)) << 8) + Long.parseLong(m.group(4)));
            }
        }
        list.forEach(a -> System.out.println(a));
    }
    // 第二题
    class BinaryTree {
        int v;
        BinaryTree left;
        BinaryTree right;

        public BinaryTree(int v) {
            this.v = v;
        }
    }
    int treeDepth(BinaryTree root) {
        if (null == root) return 0;
        int left = treeDepth(root.left);
        int right = treeDepth(root.right);
        return left > right ? (left + 1) : (right + 1);
    }
    @Test
    public void testTreeDepth() {
        BinaryTree root = new BinaryTree(1);
        root.left = new BinaryTree(2);
        root.left.left = new BinaryTree(4);
        root.left.right = new BinaryTree(5);
        root.left.right.left = new BinaryTree(6);
        root.left.right.left.right = new BinaryTree(13);

        root.right = new BinaryTree(3);
        root.right.right = new BinaryTree(7);
        root.right.right.left = new BinaryTree(8);
        root.right.right.left.left = new BinaryTree(9);
        root.right.right.left.left.left = new BinaryTree(10);
        root.right.right.left.left.right = new BinaryTree(11);
        root.right.right.left.left.left.right = new BinaryTree(12);

        System.out.println(treeDepth(root));

    }

    int[] getUnsortedArray(int len) {
        int[] array = new int[len];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(20);
        }
        return array;
    }

    @Test
    public void bubbleSort() {
        int[] array = getUnsortedArray(10);
        int[] ints = BubbleSort(array, array.length);
        System.out.println(ints);
    }

    public static int[] BubbleSort(int []arrays,int n){
        if(n==1){
            return arrays;
        }
        for(int j=0;j<n-1;j++){
            if(arrays[j]<arrays[j+1]){
                arrays[j]=arrays[j]^arrays[j+1];
                arrays[j+1]=arrays[j]^arrays[j+1];
                arrays[j]=arrays[j]^arrays[j+1];
            }
        }
        return BubbleSort(arrays,n-1);
    }

    @Test
    public void testRandom() {
        Random random = new Random(10);
        int pseudorandom = random.nextInt(50);// 区间[0,50)
        for (int i = 0; i < 5; i++) {
            System.out.println(random.nextInt(26));
        }
        System.out.println("///////////////");
        for (int i = 0; i < 5; i++) {
            System.out.println(random.nextDouble());
        }
        for (int i = 0; i < 5; i++) {
            System.out.println(random.nextDouble());
        }
        System.out.println("-----------------");
        for (int i = 0; i < 5; i++) {
            System.out.println((int)(random() * 10));
        }
        System.out.println("*****************");
        for (int i = 0; i < 5; i++) {
            System.out.println((int)(random() * 10));
        }

    }

    @Test
    public void testLong() {
        HashMap<Object, Object> hashMap = new HashMap<>();
        Hashtable<Object, Object> hashtable = new Hashtable<>();
        hashMap.put(null, null);
        System.out.println("--------------");
        hashtable.put(null, "null");
    }

    @Test
    public void testString() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        System.out.println(classLoader);
    }


    @Test
    public void testMap() {
        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();
        Map<Object, Object> synchronizedMap = Collections.synchronizedMap(objectObjectHashMap);
    }


    public int counter = 0;
    public void getA(String str) {
        if (StringUtils.isEmpty(str)) return;
        int i = str.indexOf("A");
        if (i != -1) {
            counter++;
            getA(str.substring(i+1));
        } else {
            return;
        }
    }

    @Test
    public void counter() {
        getA("jkljgAjklfdjAjklceA4343fAfdvdA");
        System.out.println(counter);
    }

    List<?super Integer> list = new ArrayList<Number>();
    String str = "user_name";

    interface MyTest{
        default void run(){
            System.out.println("default");
        };
    }

    class MyTestImpl implements MyTest{

    }

    @Test
    public void testInterface() {
        // final String result = "测试结果";
        String result = "测试结果";
        // 原始的匿名内部类
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(result);
            }
        });

        new Thread(()-> System.out.println(result));
    }

    @Test
    public void testStream() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);

        // 未使用stream和lambda表达式
        ArrayList<Integer> temp = new ArrayList<>();
        for (Integer integer : list) {
            if (integer < 4) temp.add(integer);
        }
        for (Integer integer : temp) {
            System.out.println(integer);
        }

        // 使用
        list.stream().filter(integer -> {
            if (integer < 4) return true;
            return false;
        }).forEach(integer -> System.out.println(integer));
    }

    @Test
    public void testOption() {
        String str = "True";

        System.out.println(Optional.ofNullable(str).orElse("False"));// True
        System.out.println(Optional.ofNullable(null).orElse("False"));// False
    }

    @Test
    public void testObjectTransmit() {
        User user = new User();
        user.setUsername("one");
        setUsername(user);
        System.out.println(user.getUsername());
        System.out.println("----------------");
        String str = "str";
        setString(str);
        System.out.println(str);
        System.out.println("----------------");
        User user2 = new User();
        user2.setUsername("user2");
        setUser(user2);
        System.out.println(user2.getUsername());
    }

    void setString(String str) {
        str = "str2";
        System.out.println(str);
    }

    void setUsername(User user) {
        user.setUsername("two");
        System.out.println(user.getUsername());
    }

    void setUser(User user) {
        user = new User();
        System.out.println(user.getUsername());
    }

}
