import org.junit.Test;

import java.util.concurrent.*;

public class TestThread {

    class MyThread extends Thread{
        @Override
        public void run() {
            System.out.println(Thread.currentThread() + "\r" + "test");
        }
    }

    class MyCall implements Callable<String>{

        @Override
        public String call() throws Exception {
            System.out.println("方法正在执行...");
            Thread.sleep(5000);
            return "方法执行完成！";
        }
    }

    class MyRun implements Runnable{

        @Override
        public void run() {
            System.out.println("方法正在执行......");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void testCreateThread() throws Exception {
        // Thread thread = new MyThread();
        MyCall myCall = new MyCall();
        MyRun myRun = new MyRun();
        FutureTask<String> result = new FutureTask<>(myRun, "执行结果？");
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        executorService.submit(result);
        System.out.println(result.get());
        executorService.shutdown();
    }
}
