package com.yq.cache;

import com.yq.util.SpringContextHelper;
import org.apache.ibatis.cache.Cache;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.UUID;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class RedisCache implements Cache {

    private String id;

    private RedisTemplate redisTemplate;

    private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public RedisCache(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public void putObject(Object key, Object value) {
        getRedisTemplate().opsForValue().set(key, value);
    }

    @Override
    public Object getObject(Object key) {
        return getRedisTemplate().opsForValue().get(key);
    }

    @Override
    public Object removeObject(Object key) {
        Object obj = getRedisTemplate().opsForValue().get(key);
        if (obj != null) getRedisTemplate().delete(key);
        return obj;

    }

    @Override
    public void clear() {
        getRedisTemplate().execute((RedisCallback) connection -> {
            connection.flushDb();
            return "";
        });
    }

    @Override
    public int getSize() {
        return Math.toIntExact((Long) getRedisTemplate().execute((RedisCallback<Long>) conn -> conn.dbSize()));
    }

    @Override
    public ReadWriteLock getReadWriteLock() {
        return readWriteLock;
    }

    private RedisTemplate getRedisTemplate() {
        if (redisTemplate == null) {
            redisTemplate = (RedisTemplate) SpringContextHelper.getBean("redisTemplate");
        }
        return redisTemplate;

    }
}
