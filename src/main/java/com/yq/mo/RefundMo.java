package com.yq.mo;

/**
 * created by wb-yq264139 on 2018/4/27
 */
public class RefundMo {
    public Long id;
    public String cert;
    public String uid;
    public String description;
    public String submit_date;
    public Integer is_urgent;
    public Integer is_renew;
    public Integer is_issued;
    public Integer is_completed;
}
