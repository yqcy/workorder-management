package com.yq.mo;

import java.util.ArrayList;
import java.util.List;

/**
 * created by wb-yq264139 on 2018/4/27
 */
public class RefundPageMo {
    public int total = 0;
    public List<RefundMo> refunds = new ArrayList<>();
    public int currentPage = 1;
    public int totalPage = 1;
}
