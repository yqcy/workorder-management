package com.yq.view;

import com.yq.entity.Category;
import com.yq.entity.Keyword;
import com.yq.service.CategoryService;
import com.yq.service.KeywordService;
import com.yq.service.WorkorderService;
import com.yq.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.UUID;

/**
 * created by wb-yq264139 on 2017/11/10
 */
@Controller
public class ThymeleafViewHandler {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private KeywordService keywordService;

    @Autowired
    private WorkorderService workorderService;

    @Autowired
    private DateUtils dateUtils;

    // @RequestMapping(value = "/index.html")
    // public String index(Model model) {
    //     List<Workorder> list = this.workorderService.query((Integer) null, (Integer) null);
    //     model.addAttribute("workorders", list);
    //     return "index";
    // }

    @RequestMapping(value = "/workorder_add.html")
    public String workorderAdd(Model model) {
        List<Category> categories = this.categoryService.query(null, null);
        List<Keyword> keywords = this.keywordService.queryCommonKeywords(null, null);
        model.addAttribute("categories", categories);
        model.addAttribute("keywords", keywords);
        String token = UUID.randomUUID().toString();
        HttpSession session = (HttpSession) RequestContextHolder.getRequestAttributes().getSessionMutex();
        session.setAttribute("token", token);// 添加到session中
        model.addAttribute("token", token);// 在页面中预留，与session中的token作对比，一致则通过，然后清空token
        return "workorder_add";
    }

    @RequestMapping(value = "/highcharts.html")
    public String highcharts() {
        //TODO 改成JQuery Ajax方式请求
        return "highcharts";
    }
}
