package com.yq.service;

import com.google.common.collect.Lists;
import com.yq.dao.KeywordMapper;
import com.yq.dao.WorkorderKeywordRelationMapper;
import com.yq.entity.Keyword;
import com.yq.entity.KeywordGroup;
import com.yq.enu.KeywordType;
import com.yq.mo.KeywordGroupMo;
import com.yq.util.DateUtils;
import com.yq.util.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * created by wb-yq264139 on 2017/11/10
 */
@Service
public class KeywordService {

    @Autowired
    private DateUtils dateUtils;

    @Autowired
    private PageUtils pageUtils;

    @Autowired
    private KeywordMapper keywordMapper;

    @Autowired
    private WorkorderKeywordRelationMapper workorderKeywordRelationMapper;

    public Keyword save(Keyword keyword) {
        Date date = this.dateUtils.currentTime();
        keyword.setCreateTime(date);
        keyword.setUpdateTime(date);
        this.keywordMapper.insert(keyword);
        return keyword;
    }

    public Keyword query(Keyword keyword) {
        Keyword result = this.keywordMapper.select(keyword);
        return result;
    }

    public List<Keyword> query(Integer pageNum, Integer pageSize, String type) {
        return this.keywordMapper.selectAll(this.pageUtils.compute(pageNum, pageSize), pageSize, type);
    }

    public List<Keyword> queryCommonKeywords(Integer pageNum, Integer pageSize) {
        return this.query(pageNum, pageSize, KeywordType.Common.value);
    }

    public List<Keyword> queryCustomerTypeKeywords(Integer pageNum, Integer pageSize) {
        return this.query(pageNum, pageSize, KeywordType.Customer.value);
    }

    public void remove(Long id) {
        this.keywordMapper.delete(id);
    }

    /**
     * 查询限定期限内，反馈量最高的关键词
     *
     * @param begin
     * @param end
     * @param limit 限定条数，默认10
     * @return
     */
    public List<KeywordGroupMo> queryRank(Date begin, Date end, Integer limit) {
        // List<KeywordGroup> keywordGroups = keywordMapper.selectJoinRelation(begin, end, limit);

        // 排除cert_type和customer_level类型的
        List<Keyword> customerKeywords = keywordMapper.selectAll(null, null, KeywordType.Customer.value);
        List<Keyword> certKeywords = keywordMapper.selectAll(null, null, KeywordType.Cert.value);

        ArrayList<Long> keywords = Lists.newArrayList();

        if (!CollectionUtils.isEmpty(customerKeywords)) {
            for (Keyword keyword : customerKeywords) {
                keywords.add(keyword.getId());
            }
        }
        if (!CollectionUtils.isEmpty(certKeywords)) {
            for (Keyword keyword : certKeywords) {
                keywords.add(keyword.getId());
            }
        }

        List<KeywordGroup> keywordGroups = keywordMapper.selectJoinRelationWithoutFollowKeywords(begin, end, limit, keywords);


        List<KeywordGroupMo> keywordGroupMos = new ArrayList<KeywordGroupMo>();
        if (!CollectionUtils.isEmpty(keywordGroups)) {
            for (KeywordGroup keywordGroup : keywordGroups) {
                keywordGroupMos.add(transfer4KeywordGroupMo(keywordGroup));
            }
        }

        return keywordGroupMos;
    }

    private KeywordGroupMo transfer4KeywordGroupMo(KeywordGroup keywordGroup) {
        KeywordGroupMo keywordGroupMo = new KeywordGroupMo();
        keywordGroupMo.id = keywordGroup.getId();
        keywordGroupMo.name = keywordGroup.getName();
        keywordGroupMo.count = keywordGroup.getKeywordCount();
        keywordGroupMo.create_date = dateUtils.format(keywordGroup.getCreateTime(), "yyyy-MM-dd HH:mm:ss");
        keywordGroupMo.update_date = dateUtils.format(keywordGroup.getUpdateTime(), "yyyy-MM-dd HH:mm:ss");
        return keywordGroupMo;
    }

    public List<Keyword> queryCertTypeKeywords(Integer pageNum, Integer pageSize) {
        return this.keywordMapper.selectAll(pageNum, pageSize, KeywordType.Cert.value);
    }
}
