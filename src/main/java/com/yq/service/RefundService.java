package com.yq.service;

import com.yq.dao.RefundMapper;
import com.yq.entity.Refund;
import com.yq.mo.RefundMo;
import com.yq.mo.RefundPageMo;
import com.yq.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

/**
 * created by wb-yq264139 on 2018/4/27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class RefundService {

    @Autowired
    private RefundMapper refundMapper;

    @Autowired
    private DateUtils dateUtils;

    public void add(Refund refund) {

        checkFields(refund);

        int i = refundMapper.insert(refund);

        if (i != 1) {
            // TODO ERROR
        }

    }

    public RefundPageMo queryAll(Integer page, Integer size) {

        if (page < 1 || size < 1) {
            // TODO ERROR
        }

        HashMap<Object, Object> params = new HashMap<>();

        params.put("isCompleted", 0);

        int count = refundMapper.selectCount(params);

        int index = (page - 1) * size;

        if (index >= count) {
            // TODO ERROR
        }

        params.put("column", "id");
        params.put("order", "asc");
        params.put("index", index);
        params.put("limit", size);
        List<Refund> refunds = refundMapper.selectList(params);

        RefundPageMo refundPageMo = new RefundPageMo();


        if (!CollectionUtils.isEmpty(refunds)) {
            refundPageMo.total = count;
            for (Refund refund : refunds) {
                refundPageMo.refunds.add(transfer2Mo(refund));
            }
            refundPageMo.currentPage = page;
            refundPageMo.totalPage = (refundPageMo.total % size == 0) ? refundPageMo.total / size : refundPageMo.total / size + 1;
        }

        return refundPageMo;
    }

    private RefundMo transfer2Mo(Refund refund) {
        RefundMo refundMo = new RefundMo();
        refundMo.id = refund.getId();
        refundMo.cert = refund.getCertInstance();
        refundMo.uid = refund.getUserId();
        refundMo.description = refund.getDescription();
        refundMo.submit_date = dateUtils.format(refund.getCreateTime(), "yyyy-MM-dd HH:mm");
        refundMo.is_urgent = refund.getIsUrgent();
        refundMo.is_renew = refund.getIsRenew();
        refundMo.is_issued = refund.getIsIssued();
        refundMo.is_completed = refund.getIsCompleted();
        return refundMo;
    }

    private void checkFields(Refund refund) {

        Date currentTime = dateUtils.currentTime();

        if (refund.getCreateTime() == null) {
            refund.setCreateTime(currentTime);
        }

        if (refund.getUpdateTime() == null) {
            refund.setUpdateTime(currentTime);
        }

        if (refund.getIsUrgent() == null) {
            refund.setIsUrgent(0);
        }
        if (refund.getIsRenew() == null) {
            refund.setIsRenew(0);
        }
        if (refund.getIsIssued() == null) {
            refund.setIsIssued(0);
        }
        if (refund.getIsUrgent() == null) {
            refund.setIsUrgent(0);
        }

        if (!Pattern.matches("^cas(-cn)?-([a-zA-Z0-9]){12}$", refund.getCertInstance())) {
            throw new RuntimeException("证书实例格式不正确！");
        }

        if (!Pattern.matches("^[0-9]+$", refund.getUserId())) {
            throw new RuntimeException("UID格式不正确！");
        }
    }

    public void modify(Refund refund) {
        int i = refundMapper.update(refund);
        if (i != 1) {
            // TODO ERROR
        }
    }

    public void modifyIssued(Long refundId, Integer isIssued) {
        Refund refund = new Refund();
        refund.setId(refundId);
        refund.setIsIssued(isIssued);
        modify(refund);
    }

    public void modifyRenew(Long refundId, Integer isRenew) {
        Refund refund = new Refund();
        refund.setId(refundId);
        refund.setIsRenew(isRenew);
        modify(refund);
    }

    public void modifyUrgent(Long refundId, Integer isUrgent) {
        Refund refund = new Refund();
        refund.setId(refundId);
        refund.setIsUrgent(isUrgent);
        modify(refund);
    }

    /**
     * 查询优先退款
     *
     * @param page
     * @param size
     * @return
     */
    public RefundPageMo queryPrior(Integer page, Integer size) {
        if (page == null || size == null) {
            throw new RuntimeException("invalid params: page and size !");
        }
        HashMap<Object, Object> params = new HashMap<>();
        params.put("isCompleted", 0);
        params.put("isUrgent", 1);
        int count = refundMapper.selectCount(params);

        int index = (page - 1) * size;

        if (index >= count) {
            throw new RuntimeException("out of the range: prior !");
        }

        params.put("column", "id");
        params.put("order", "asc");
        params.put("index", index);
        params.put("limit", size);

        List<Refund> refunds = refundMapper.selectList(params);

        RefundPageMo refundPageMo = new RefundPageMo();


        if (!CollectionUtils.isEmpty(refunds)) {
            refundPageMo.total = count;
            for (Refund refund : refunds) {
                refundPageMo.refunds.add(transfer2Mo(refund));
            }
            refundPageMo.currentPage = page;
            refundPageMo.totalPage = (refundPageMo.total % size == 0) ? refundPageMo.total / size : refundPageMo.total / size + 1;
        }

        return refundPageMo;
    }

    public void modifyCompleted(Long refundId, Integer isCompleted) {
        Refund refund = new Refund();
        refund.setId(refundId);
        refund.setIsCompleted(isCompleted);
        int i = refundMapper.update(refund);
        if (i != 1) {
            throw new RuntimeException("update error !");
        }
    }

    public RefundPageMo getByCertInstance(String certInstance, Integer page, Integer pageSize) {
        int count = refundMapper.selectCountByCert(certInstance);
        if (count > 0) {
            RefundPageMo refundPageMo = new RefundPageMo();
            refundPageMo.total = count;
            List<Refund> refunds = refundMapper.selectByCert(certInstance);
            if (!CollectionUtils.isEmpty(refunds)) {
                for (Refund refund : refunds) {
                    refundPageMo.refunds.add(transfer2Mo(refund));
                }
            }
            refundPageMo.currentPage = page;
            refundPageMo.totalPage = (refundPageMo.total % pageSize == 0) ? refundPageMo.total / pageSize : refundPageMo.total / pageSize + 1;
            return refundPageMo;
        }
        return null;
    }
}
