package com.yq.service;

public interface UserService {
    boolean login(String username, String password);

    boolean logout(String username);
}
