package com.yq.service.impl;

import com.yq.dao.UserMapper;
import com.yq.service.UserService;
import com.yq.util.SpringContextHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

@Service
public class UserServiceImpl implements UserService {

    private HttpSession httpSession;

    @Autowired
    private UserMapper userMapper;

    @Override
    public boolean login(String username, String password) {

        return false;
    }

    @Override
    public boolean logout(String username) {
        return false;
    }

    private HttpSession getSession() {
        if (httpSession != null) {
            return httpSession;
        }
        httpSession = SpringContextHelper.getBean(HttpSession.class);
        return httpSession;
    }
}
