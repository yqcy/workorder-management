package com.yq.conf;

import com.yq.interceptor.ControllerRequestLogInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * created by YQ on 2017-12-24
 */
@Configuration
public class WebInterceptor extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new ControllerRequestLogInterceptor()).addPathPatterns("/category/**").addPathPatterns("/keyword/**").addPathPatterns("/workorder/**");
        super.addInterceptors(registry);
    }
}
