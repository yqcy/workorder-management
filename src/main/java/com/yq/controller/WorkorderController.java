package com.yq.controller;

import com.yq.entity.Keyword;
import com.yq.entity.Workorder;
import com.yq.util.ExcelUtils;
import com.yq.service.WorkorderService;
import com.yq.util.DateUtils;
import io.swagger.annotations.*;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * created by wb-yq264139 on 2017/11/10
 */
@Api(value = "API - WorkorderController", description = "工单接口")
@RestController
@RequestMapping(value = "/workorder")
public class WorkorderController {

    @Autowired
    private DateUtils dateUtils;

    @Autowired
    private WorkorderService workorderService;

    @Autowired
    private ExcelUtils excelUtils;

    @ApiOperation(value = "添加新的处理工单", notes = "支持POST方式", response = String.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "number", value = "工单号", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "category", value = "类别的主键", required = false, dataType = "Long", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "keywords", value = "关键词的主键集合", required = false, dataType = "Long", paramType = "query", defaultValue = "1")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "请求已完成"),
            @ApiResponse(code = 400, message = "请求中有语法问题，或不能满足请求"),
            @ApiResponse(code = 401, message = "未授权客户机访问数据"),
            @ApiResponse(code = 403, message = "服务器资源不可用，或无权访问"),
            @ApiResponse(code = 404, message = "服务器找不到给定的资源，或文档不存在"),
            @ApiResponse(code = 500, message = "服务器不能完成请求")}
    )
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Object addWorkorder(@RequestParam(value = "number") String number,
                               @RequestParam(value = "category", required = false) Long categoryId,
                               @RequestParam(value = "keywords", required = false) List<Long> keywords,
                               @RequestParam(value = "token") String token){
        // 首先对比token看是否为重复提交，或者是多表单提交
        // 这两种情况都是不允许的
        HttpSession session = (HttpSession) RequestContextHolder.currentRequestAttributes().getSessionMutex();
        String sourceToken = (String) session.getAttribute("token");
        if (StringUtils.isEmpty(token)) {
            throw new RuntimeException("token can not be empty !");
        }

        if (!token.equals(sourceToken)) {
            throw new RuntimeException("can not match the source token !");
        } else {
            session.setAttribute("token", null);
        }

        Workorder workorder = new Workorder(number.trim(), categoryId);
        return this.workorderService.save(workorder, keywords);
    }

    @ApiOperation(value = "查询某个工单", notes = "支持GET方式", response = String.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = false, dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "number", value = "工单号", required = false, dataType = "String", paramType = "query")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "请求已完成"),
            @ApiResponse(code = 400, message = "请求中有语法问题，或不能满足请求"),
            @ApiResponse(code = 401, message = "未授权客户机访问数据"),
            @ApiResponse(code = 403, message = "服务器资源不可用，或无权访问"),
            @ApiResponse(code = 404, message = "服务器找不到给定的资源，或文档不存在"),
            @ApiResponse(code = 500, message = "服务器不能完成请求")}
    )
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public Object queryWorkorder(@RequestParam(value = "id", required = false) Long id,
                                 @RequestParam(value = "number", required = false) String number) {
        Workorder keyword = new Workorder(id, number);
        return this.workorderService.query(keyword);
    }

    @ApiOperation(value = "查询多个工单", notes = "支持GET方式", response = String.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页号", required = false, dataType = "Int", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页的条数", required = false, dataType = "Int", paramType = "query", defaultValue = "10")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "请求已完成"),
            @ApiResponse(code = 400, message = "请求中有语法问题，或不能满足请求"),
            @ApiResponse(code = 401, message = "未授权客户机访问数据"),
            @ApiResponse(code = 403, message = "服务器资源不可用，或无权访问"),
            @ApiResponse(code = 404, message = "服务器找不到给定的资源，或文档不存在"),
            @ApiResponse(code = 500, message = "服务器不能完成请求")}
    )
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Object queryWorkorders(@RequestParam(value = "page", required = false) Integer pageNum,
                                  @RequestParam(value = "size", required = false) Integer pageSize) {
        return this.workorderService.query(pageNum, pageSize);
    }

    @ApiOperation(value = "删除某个工单", notes = "支持POST方式", response = String.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Long", paramType = "query")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "请求已完成"),
            @ApiResponse(code = 400, message = "请求中有语法问题，或不能满足请求"),
            @ApiResponse(code = 401, message = "未授权客户机访问数据"),
            @ApiResponse(code = 403, message = "服务器资源不可用，或无权访问"),
            @ApiResponse(code = 404, message = "服务器找不到给定的资源，或文档不存在"),
            @ApiResponse(code = 500, message = "服务器不能完成请求")}
    )
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Object removeWorkorder(@RequestParam(value = "id") Long id) {
        this.workorderService.remove(id);
        return "success";
    }

    @ApiOperation(value = "下载Excel文档", notes = "支持GET方式", response = String.class)
    @ApiImplicitParams({})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "请求已完成"),
            @ApiResponse(code = 400, message = "请求中有语法问题，或不能满足请求"),
            @ApiResponse(code = 401, message = "未授权客户机访问数据"),
            @ApiResponse(code = 403, message = "服务器资源不可用，或无权访问"),
            @ApiResponse(code = 404, message = "服务器找不到给定的资源，或文档不存在"),
            @ApiResponse(code = 500, message = "服务器不能完成请求")}
    )
    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public ResponseEntity<byte[]> download() {
        Date date = this.dateUtils.currentTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime() - (7 * 24 * 60 * 60 * 1000));
        Date preDate = calendar.getTime();
        List<Workorder> workorders = this.workorderService.query(preDate, date);
        Workbook wb = this.excelUtils.createWorkbook();
        Sheet sheet = this.excelUtils.createSheet(wb, "工单上周处理情况");
        this.excelUtils.setTitleColumnWidth(sheet);
        Row titleRow = this.excelUtils.createRow(sheet, 0);
        Cell cell1 = this.excelUtils.createCell(titleRow, 0);
        Cell cell2 = this.excelUtils.createCell(titleRow, 1);
        Cell cell3 = this.excelUtils.createCell(titleRow, 2);
        Cell cell4 = this.excelUtils.createCell(titleRow, 3);
        Cell cell5 = this.excelUtils.createCell(titleRow, 4);
        CellStyle titleCellStyle = this.excelUtils.getTitleCellStyle(wb);
        this.excelUtils.setTitleFont(wb, titleCellStyle);
        this.excelUtils.setValue(cell1, titleCellStyle, "工单号");
        this.excelUtils.setValue(cell2, titleCellStyle, "工单类型");
        this.excelUtils.setValue(cell3, titleCellStyle, "工单关键字");
        this.excelUtils.setValue(cell4, titleCellStyle, "工单创建时间");
        this.excelUtils.setValue(cell5, titleCellStyle, "工单更新时间");
        CellStyle contentCellStyle = this.excelUtils.getContentCellStyle(wb);
        this.excelUtils.setContentFont(wb, contentCellStyle);
        for (int i = 0; i < workorders.size(); i++) {
            Workorder workorder = workorders.get(i);
            Row row = this.excelUtils.createRow(sheet, i + 1);
            Cell c1 = this.excelUtils.createCell(row, 0);
            Cell c2 = this.excelUtils.createCell(row, 1);
            Cell c3 = this.excelUtils.createCell(row, 2);
            Cell c4 = this.excelUtils.createCell(row, 3);
            Cell c5 = this.excelUtils.createCell(row, 4);
            this.excelUtils.setValue(c1, contentCellStyle, workorder.getNumber());
            this.excelUtils.setValue(c2, contentCellStyle, workorder.getCategory().getName());
            //转成name数组
            List<Keyword> keywords = workorder.getKeywords();
            StringBuilder builder = new StringBuilder();
            if (keywords == null || keywords.size() == 0) {
                builder.append("无");
            } else {
                for (Keyword keyword : keywords) {
                    builder.append(keyword.getName()).append(",");
                }
                builder.deleteCharAt(builder.length() - 1);
            }
            this.excelUtils.setValue(c3, contentCellStyle, builder.toString());
            this.excelUtils.setValue(c4, contentCellStyle, workorder.getCreateTime());
            this.excelUtils.setValue(c5, contentCellStyle, workorder.getUpdateTime());
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        this.excelUtils.write(wb, outputStream);
        byte[] bytes = outputStream.toByteArray();
        HttpHeaders httpHeaders = new HttpHeaders();
        String dateFormat = this.dateUtils.format(this.dateUtils.currentTime(), "yyyyMMddHHmmss");
        String fileName = "";
        try {
            fileName = new String(("工单处理表" + dateFormat + ".xls").getBytes("utf-8"), "iso-8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //TODO 编码格式转换异常
        }
        httpHeaders.add("Content-Disposition", "attchement;filename=" + fileName);
        return new ResponseEntity(bytes, httpHeaders, HttpStatus.OK);
    }

    @ApiOperation(value = "获得token", notes = "支持GET方式", response = String.class)
    @ApiImplicitParams({
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "请求已完成"),
            @ApiResponse(code = 400, message = "请求中有语法问题，或不能满足请求"),
            @ApiResponse(code = 401, message = "未授权客户机访问数据"),
            @ApiResponse(code = 403, message = "服务器资源不可用，或无权访问"),
            @ApiResponse(code = 404, message = "服务器找不到给定的资源，或文档不存在"),
            @ApiResponse(code = 500, message = "服务器不能完成请求")}
    )
    @RequestMapping(value = "/token", method = RequestMethod.GET)
    public Object setToken() {
        String token = UUID.randomUUID().toString();
        HttpSession session = (HttpSession) RequestContextHolder.currentRequestAttributes().getSessionMutex();
        session.setAttribute("token", token);
        return token;
    }

}
