package com.yq.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yq.entity.Refund;
import com.yq.service.RefundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * created by wb-yq264139 on 2018/4/27
 */
@RestController
@RequestMapping("/refund")
public class RefundController {

    @Autowired
    private RefundService refundService;

    @RequestMapping(value = "/prior", method = RequestMethod.GET)
    public String priorRefund(@RequestParam(value = "page") Integer page,
                              @RequestParam(value = "size") Integer size) {
        return JSON.toJSONString(refundService.queryPrior(page, size));
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public String all(@RequestParam(value = "page") Integer page,
                      @RequestParam(value = "size") Integer size) {
        return JSON.toJSONString(refundService.queryAll(page, size));
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@RequestParam(value = "cert") String certInstance,
                      @RequestParam(value = "uid") String userId,
                      @RequestParam(value = "desc") String description,
                      @RequestParam(value = "renew") Integer isRenew,
                      @RequestParam(value = "issued") Integer isIssued,
                      @RequestParam(value = "urgent") Integer isUrgent) {
        Refund refund = new Refund(certInstance, userId, description, isRenew, isIssued, isUrgent);
        refundService.add(refund);
        return "success";
    }

    @RequestMapping(value = "/modify/urgent", method = RequestMethod.POST)
    public String modifyUrgent(@RequestParam(value = "id") Long refundId,
                               @RequestParam(value = "is_urgent") Integer isUrgent) {
        refundService.modifyUrgent(refundId, isUrgent);
        return "success";
    }

    @RequestMapping(value = "/modify/renew", method = RequestMethod.POST)
    public String modifyRenew(@RequestParam(value = "id") Long refundId,
                              @RequestParam(value = "is_renew") Integer isRenew) {
        refundService.modifyRenew(refundId, isRenew);
        return "success";
    }


    @RequestMapping(value = "/modify/issued", method = RequestMethod.POST)
    public String modifyIssued(@RequestParam(value = "id") Long refundId,
                               @RequestParam(value = "is_issued") Integer isIssued) {
        refundService.modifyIssued(refundId, isIssued);
        return "success";
    }

    @RequestMapping(value = "/modify/completed", method = RequestMethod.POST)
    public String modifyCompleted(@RequestParam(value = "id") Long refundId,
                                  @RequestParam(value = "is_completed") Integer isCompleted) {
        refundService.modifyCompleted(refundId, isCompleted);
        return "success";
    }

    @RequestMapping(value = "/get/cert", method = RequestMethod.GET)
    public String queryByCertInstance(@RequestParam(value = "cert") String certInstance,
                                      @RequestParam(value = "page") Integer page,
                                      @RequestParam(value = "size") Integer pageSize) {
        return JSONObject.toJSONString(refundService.getByCertInstance(certInstance, page, pageSize));
    }
}
