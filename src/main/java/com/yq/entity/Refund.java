package com.yq.entity;

/**
 * created by wb-yq264139 on 2018/4/27
 */
public class Refund extends BaseEntity{

    private String certInstance;
    private String userId;
    private String description;
    private Integer isUrgent;
    private Integer isRenew;
    private Integer isIssued;
    private Integer isCompleted;

    public Refund() {
    }



    public Refund(String certInstance, String userId, String description, Integer isRenew, Integer isIssued, Integer isUrgent) {
        this.certInstance = certInstance;
        this.userId = userId;
        this.description = description;
        this.isRenew = isRenew;
        this.isIssued = isIssued;
        this.isUrgent = isUrgent;
    }

    public String getCertInstance() {
        return certInstance;
    }

    public void setCertInstance(String certInstance) {
        this.certInstance = certInstance;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIsUrgent() {
        return isUrgent;
    }

    public void setIsUrgent(Integer isUrgent) {
        this.isUrgent = isUrgent;
    }

    public Integer getIsRenew() {
        return isRenew;
    }

    public void setIsRenew(Integer isRenew) {
        this.isRenew = isRenew;
    }

    public Integer getIsIssued() {
        return isIssued;
    }

    public void setIsIssued(Integer isIssued) {
        this.isIssued = isIssued;
    }

    public Integer getIsCompleted() {
        return isCompleted;
    }

    public void setIsCompleted(Integer isCompleted) {
        this.isCompleted = isCompleted;
    }
}
