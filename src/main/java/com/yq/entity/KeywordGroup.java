package com.yq.entity;

/**
 * created by wb-yq264139 on 2018/5/2
 */
public class KeywordGroup extends BaseEntity{
    private String name;
    private Integer keywordCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getKeywordCount() {
        return keywordCount;
    }

    public void setKeywordCount(Integer keywordCount) {
        this.keywordCount = keywordCount;
    }
}
