package com.yq.entity;

import lombok.Data;
import lombok.NonNull;

@Data
public class User {
    private String username;
    private String password;
}
