package com.yq.entity;

/**
 * created by YQ on 2017-11-06
 */
public class Category extends BaseEntity {
    private static final long serialVersionUID = 6039548603193344381L;
    private String name;

    public Category() {
    }

    public Category(String name) {
        this.name = name;
    }

    public Category(Long id, String name) {
        super.id = id;
        this.name = name;
    }

    public Category(Long categoryId) {
        super.id = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
