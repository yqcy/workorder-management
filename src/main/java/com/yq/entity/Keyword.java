package com.yq.entity;

/**
 * created by YQ on 2017-11-06
 */
public class Keyword extends BaseEntity {

    private static final long serialVersionUID = -3476413321929064207L;

    private String name;
    private String type;

    public Keyword() {
    }

    public Keyword(String name) {
        this.name = name;
        this.type = "";
    }

    public Keyword(Long id, String name) {
        super.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
