package com.yq.entity;

/**
 * created by wb-yq264139 on 2017/11/17
 */
public class Log extends BaseEntity {

    private String className;
    private String methodName;
    private String paramJson;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getParamJson() {
        return paramJson;
    }

    public void setParamJson(String paramJson) {
        this.paramJson = paramJson;
    }
}
