package com.yq.enu;

/**
 * created by wb-yq264139 on 2019/1/21
 */
public enum KeywordType {
    Customer("customer_level"),
    Cert("cert_type"),
    Common("");

    public String value;

    KeywordType(String value) {
        this.value = value;
    }
}


