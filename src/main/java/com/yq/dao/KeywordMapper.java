package com.yq.dao;

import com.yq.entity.Keyword;
import com.yq.entity.KeywordGroup;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * created by wb-yq264139 on 2017/11/10
 */
@Mapper
public interface KeywordMapper {

    void insert(Keyword keyword);

    Keyword select(@Param("keyword") Keyword keyword);

    List<Keyword> selectAll(@Param("index") Integer index, @Param("size") Integer size, @Param("type") String type);

    int count();

    void delete(Long id);

    List<KeywordGroup> selectJoinRelation(@Param("beginDate") Date beginDate, @Param("endDate") Date endDate, @Param("limit") Integer limit);

    List<KeywordGroup> selectJoinRelationWithoutFollowKeywords(@Param("beginDate") Date beginDate, @Param("endDate") Date endDate, @Param("limit") Integer limit, @Param("keywords") List<Long> keywords);
}
