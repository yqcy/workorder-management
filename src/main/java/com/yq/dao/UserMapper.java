package com.yq.dao;

import com.yq.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {
    void save(User user);

    User queryByUsername(String username);
}
