package com.yq.dao;

import com.yq.entity.Log;
import org.apache.ibatis.annotations.Mapper;

/**
 * created by wb-yq264139 on 2017/11/17
 */
@Mapper
public interface LogMapper {

    void insert(Log log);
}
