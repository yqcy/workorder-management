package com.yq.dao;

import com.yq.entity.Refund;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * created by wb-yq264139 on 2018/4/27
 */
@Mapper
public interface RefundMapper {

    int insert(Refund refund);

    List<Refund> selectList(Map params);

    int selectCount(Map params);

    int update(Refund refund);

    List<Refund> selectByCert(String certInstance);

    int selectCountByCert(String certInstance);
}
