package com.yq.aop;

import com.alibaba.fastjson.JSONObject;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * created by YQ on 2017-12-24
 */
@Aspect
@Component
public class ServiceLogAspect {

    private Logger logger = LoggerFactory.getLogger(ServiceLogAspect.class);

    @Pointcut("execution(public * com.yq.service.*.*(..))")
    public void methodInServicePointcut(){}

    @Around("methodInServicePointcut()")
    public Object methodInServiceAdvance(ProceedingJoinPoint point) throws Throwable {
        MethodInvocationProceedingJoinPoint methodPoint = (MethodInvocationProceedingJoinPoint) point;
        MethodSignature signature = (MethodSignature) methodPoint.getSignature();
        Method method = signature.getMethod();
        Class returnType = signature.getReturnType();
        Object[] args = methodPoint.getArgs();
        Object result = null;
        try {
            result = methodPoint.proceed();
        } catch (Throwable throwable) {
            logger.error("method invoke failure ! it be cause by {}", throwable);
            throw throwable;
        } finally {
            logger.info("methodName is {}, args is {}, returnType is {}, returnResult is {}", method.getName(), JSONObject.toJSONString(args), returnType.getTypeName(), result);
        }
        return result;
    }
}
