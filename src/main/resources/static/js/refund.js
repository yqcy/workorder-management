/**
 * 字符串替换
 * 参考自http://blog.csdn.net/qq_23616601/article/details/77481516
 */
String.prototype.format = function () {
    if (arguments.length == 0) return this;
    var param = arguments[0];
    var s = this;
    if (typeof(param) == 'object') {
        for (var key in param)
            s = s.replace(new RegExp("\\{" + key + "\\}", "g"), param[key]);
        return s;
    } else {
        for (var i = 0; i < arguments.length; i++)
            s = s.replace(new RegExp("\\{" + i + "\\}", "g"), arguments[i]);
        return s;
    }
}

/**
 * 优先退款
 * 提交退款申请超过3天的退款，需要考虑周末不处理的情况
 * refunds: 退款jsonArray
 */
function showPriorRefunds(refunds, page, size) {
    // 先把表格清空，防止重复append
    $('#priorRefundTbody').empty();
    // 开始追加
    $.each(refunds, function (i, e) {
        e.no = (page - 1) * size + i + 1;

        if (e.is_urgent == 1) {
            e.urgent = '<input type="checkbox" checked onchange="sendUrgent({0}, {1});"/>'.format(e.id, 0);
        } else {
            e.urgent = '<input type="checkbox" onchange="sendUrgent({0}, {1});"/>'.format(e.id, 1);
        }

        if (e.is_renew == 1) {
            e.renew = '<input type="checkbox" checked onchange="sendRenew({0}, {1});" />'.format(e.id, 0);
        } else {
            e.renew = '<input type="checkbox" onchange="sendRenew({0}, {1});" />'.format(e.id, 1);
        }

        if (e.is_issued == 1) {
            e.issued = '<input type="checkbox" checked onchange="sendIssued({0}, {1});"/>'.format(e.id, 0);
        } else {
            e.issued = '<input type="checkbox" onchange="sendIssued({0}, {1});"/>'.format(e.id, 1);
        }

        if (e.is_completed == 1) {
            e.completed = '<input type="checkbox" checked onchange="sendCompleted({0}, {1});"/>'.format(e.id, 0);
        } else {
            e.completed = '<input type="checkbox" onchange="sendCompleted({0}, {1});"/>'.format(e.id, 1);
        }

        $('#priorRefundTbody').append('<tr><td class="text-center">{no}</td><td class="text-center">{urgent}</td><td class="text-center">{renew}</td><td class="text-center">{issued}</td><td class="text-center">{completed}</td><td>{cert}</td><td>{uid}</td><td>{description}</td><td class="text-center">{submit_date}</td></tr>'.format(e));
    });
}

/**
 * 所有退款信息
 * 默认按添加顺序正序排序
 * refunds：退款jsonArray
 */
function showAllRefunds(refunds, currentPage, pageSize) {
    // 先把表格清空，防止重复append
    $('#allRefundTbody').empty();
    // 开始追加
    $.each(refunds, function (i, e) {
        e.no = (currentPage - 1) * pageSize + i + 1;

        if (e.is_urgent == 1) {
            e.urgent = '<input type="checkbox" checked onchange="sendUrgent({0}, {1});"/>'.format(e.id, 0);
        } else {
            e.urgent = '<input type="checkbox" onchange="sendUrgent({0}, {1});"/>'.format(e.id, 1);
        }

        if (e.is_renew == 1) {
            e.renew = '<input type="checkbox" checked onchange="sendRenew({0}, {1});" />'.format(e.id, 0);
        } else {
            e.renew = '<input type="checkbox" onchange="sendRenew({0}, {1});" />'.format(e.id, 1);
        }

        if (e.is_issued == 1) {
            e.issued = '<input type="checkbox" checked onchange="sendIssued({0}, {1});"/>'.format(e.id, 0);
        } else {
            e.issued = '<input type="checkbox" onchange="sendIssued({0}, {1});"/>'.format(e.id, 1);
        }

        if (e.is_completed == 1) {
            e.completed = '<input type="checkbox" checked onchange="sendCompleted({0}, {1});"/>'.format(e.id, 0);
        } else {
            e.completed = '<input type="checkbox" onchange="sendCompleted({0}, {1});"/>'.format(e.id, 1);
        }

        $('#allRefundTbody').append('<tr><td class="text-center">{no}</td><td class="text-center">{urgent}</td><td class="text-center">{renew}</td><td class="text-center">{issued}</td><td class="text-center">{completed}</td><td>{cert}</td><td>{uid}</td><td>{description}</td><td class="text-center">{submit_date}</td></tr>'.format(e));
    });
}

function checkCert(cert) {
    var certReg = /^cas(-cn)?-([a-zA-Z0-9]){12}$/;
    return certReg.test(cert);
}

function checkUID(uid) {
    var uidReg = /^[0-9]+$/;
    return uidReg.test(uid);
}

function submitForm() {
    var data = $('#form').serialize();
    var isExist = false;

    var cert = $('#cert').val();
    var uid = $('#uid').val();

    // 对证书实例、用户ID进行正则校验，防止脏数据
    if (!checkCert(cert)) {
        alert("证书实例格式不正确！");
    }
    var uidReg = /^[0-9]+$/;
    if (!checkUID(uid)) {
        alert("UID格式不正确！");
        return;
    }

    // 判断证书实例的退款是否已经添加
    $.getJSON('http://localhost:8989/refund/get/cert?cert={0}&page={1}&size={2}'.format(cert, 1, 1), function (result) {
        if (result) {
            isExist = true;
            alert("该实例的退款已经被添加！");
        }
    });

    if (!isExist) {
        $.post('http://localhost:8989/refund/add', data, function (result) {
            if (result) {
                flush();
            }
        });
    }
}

function sendUrgent(id, code) {
    $.post('http://localhost:8989/refund/modify/urgent', {id: id, is_urgent: code}, function (data) {
        if (data) {
            flush();
        }
    });
}

function sendRenew(id, code) {
    $.post('http://localhost:8989/refund/modify/renew', {id: id, is_renew: code}, function (data) {
        if (data) {
            flush();
        }
    });
}

function sendIssued(id, code) {
    $.post('http://localhost:8989/refund/modify/issued', {id: id, is_issued: code}, function (data) {
        if (data) {
            flush();
        }
    });
}

function sendCompleted(id, code) {
    $.post('http://localhost:8989/refund/modify/completed', {id: id, is_completed: code}, function (data) {
        if (data) {
            flush();
        }
    });
}

function flush() {
    window.location.reload(true);
}

function showAllRefundsPageToolbar(currentPage, totalPage, pageSize, toolbarSize) {
    $('#allPageToolbar').empty();

    if (currentPage > 1) {
        $('#allPageToolbar').append('<li><a href="javascript:void(0);" onclick="getAllRefunds({0}, {1}, {2})">&laquo;</a></li>'.format(currentPage - 1, pageSize, toolbarSize));
    }

    var begin = currentPage - ((currentPage % toolbarSize == 0) ? toolbarSize : currentPage % toolbarSize) + 1;
    var end = (begin + toolbarSize - 1 > totalPage) ? totalPage : begin + toolbarSize - 1;

    for (; begin <= end; begin++) {
        if (begin == currentPage) {
            $('#allPageToolbar').append('<li class="active"><a href="javascript:void(0);">{0}</a></li>'.format(begin));
        } else {
            $('#allPageToolbar').append('<li><a href="javascript:void(0);" onclick="getAllRefunds({1}, {2}, {3})">{0}</a></li>'.format(begin, begin, pageSize, toolbarSize));
        }
    }

    if (currentPage < totalPage) {
        $('#allPageToolbar').append('<li><a href="javascript:void(0);" onclick="getAllRefunds({0}, {1}, {2})">&raquo;</a></li>'.format(currentPage + 1, pageSize, toolbarSize));
    }
}

function showPriorRefundsPageToolbar(currentPage, totalPage, pageSize, toolbarSize) {
    $('#priorPageToolbar').empty();

    if (currentPage > 1) {
        $('#priorPageToolbar').append('<li><a href="javascript:void(0);" onclick="getPriorRefunds({0}, {1}, {2})">&laquo;</a></li>'.format(currentPage - 1, pageSize, toolbarSize));
    }

    var begin = currentPage - ((currentPage % toolbarSize == 0) ? toolbarSize : currentPage % toolbarSize) + 1;
    var end = (begin + toolbarSize - 1 > totalPage) ? totalPage : begin + toolbarSize - 1;

    for (; begin <= end; begin++) {
        if (begin == currentPage) {
            $('#priorPageToolbar').append('<li class="active"><a href="javascript:void(0);">{0}</a></li>'.format(begin));
        } else {
            $('#priorPageToolbar').append('<li><a href="javascript:void(0);" onclick="getPriorRefunds({1}, {2}, {3})">{0}</a></li>'.format(begin, begin, pageSize, toolbarSize));
        }
    }

    if (currentPage < totalPage) {
        $('#priorPageToolbar').append('<li><a href="javascript:void(0);" onclick="getPriorRefunds({0}, {1}, {2})">&raquo;</a></li>'.format(currentPage + 1, pageSize, toolbarSize));
    }
}

function getAllRefunds(page, pageSize, pageToolbarSize) {
    $.getJSON('http://localhost:8989/refund/all?page={0}&size={1}'.format(page, pageSize), function (data) {
        if (data != null) {
            showAllRefunds(data.refunds, data.currentPage, pageSize);
            // 初始化所有退款表格的分页
            showAllRefundsPageToolbar(data.currentPage, data.totalPage, pageSize, pageToolbarSize);

            $('#total-count').text(data.total);
        }
    });
}

function getPriorRefunds(page, pageSize, pageToolbarSize) {
    // 初始化优先退款表格
    $.getJSON('http://localhost:8989/refund/prior?page={0}&size={1}'.format(page, pageSize), function (data) {
        if (data != null) {
            showPriorRefunds(data.refunds, data.currentPage, pageSize);
            showPriorRefundsPageToolbar(data.currentPage, data.totalPage, pageSize, pageToolbarSize);

            $('#prior-count').text(data.total);
        }
    });
}

function queryRefunds() {
    var cert = $('#input_cert').val();

    if (cert == '') {
        flush();
    }

    $.getJSON('http://localhost:8989/refund/get/cert?cert={0}&page={1}&size={2}'.format(cert, 1, 5), function (data) {
        if (data) {
            showAllRefunds(data.refunds, data.currentPage, 10);
            showAllRefundsPageToolbar(data.currentPage, 1, 10, 1);// 默认toolbar size = 5
        } else {
            $('#allRefundTbody').empty();
            $('#allPageToolbar').empty();
        }
    });

}

$(function () {

    var defaultPageSize = 5;

    var defaultPageToolbarSize = 5;// 这个至少应该大于等于2

    // 初始化所有退款表格
    getAllRefunds(1, defaultPageSize, defaultPageToolbarSize);

    getPriorRefunds(1, defaultPageSize, defaultPageToolbarSize);
});
