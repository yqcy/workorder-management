# 工单处理记录管理系统

## 项目启动

1. 配置DB，使用 **create_tables.sql** 创建库表；
2. 修改 **application.properties** 中的DataSource配置；
3. 运行 **ApplicationStart** 的main方法。

## 访问路径

- 添加工单处理：[http://localhost:8989/workorder_add.html](http://localhost:8989/workorder_add.html)
- 查看Swagger-UI：[http://localhost:8989/swagger-ui.html](http://localhost:8989/swagger-ui.html)
- 查看Druid监控界面：[http://localhost:8989/druid/index.html](http://localhost:8989/druid/index.html)
- 查看工单处理详细类别比例图（包括类别、关键词）：[http://localhost:8989/workorder_detail.html](http://localhost:8989/workorder_detail.html)
- 下载工单处理情况的Excel表格：[http://localhost:8989/workorder/download](http://localhost:8989/workorder/download)

## 包结构

| 包名 | 注释 |
| --- | --- |
| conf | 配置层，存放JavaConfigBean |
| controller | 前后端交互层，一般以JSON数据返回 | 
| dao | 数据持久层 |
| entity | 实体层 |
| service | 服务层，处理代码业务逻辑 |
| util | 工具层，注意：这里的工具使用Bean形式，不要以静态方法形式 |
| view | 视图控制层，负责页面的切换 |

## 资源resources结构

| 目录名 | 备注 |
| --- | --- |
| mapper | mybatis的.xml存储目录 |
| public | html页面存放目录 |
| templates | 模板页面存放目录 |

## 功能需求

1. 关键词排行榜添加时间区间查询，例如一周内的关键词排行；
2. 添加退款可视化界面，代替sublime记事本；